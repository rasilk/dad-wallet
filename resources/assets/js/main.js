require("./bootstrap");

import Vue from "vue";
import App from "./App";
import router from "./router/index";
import store from "./stores/global-store"

import PaperDashboard from "./plugins/paperDashboard";
import "vue-notifyjs/themes/default.css";

import BootstrapVue from 'bootstrap-vue'
import axios from 'axios'

//Vue.prototype.$http = axios

import VueSocketIO from "vue-socket.io";

Vue.use(
    new VueSocketIO({
        debug: true,
        connection: "http://127.0.0.1:8080"
    })
);


Vue.use(BootstrapVue)

Vue.use(PaperDashboard);

Vue.component('pagination', require('laravel-vue-pagination'));

Vue.mixin({
    methods: {
        successToast(text) {
            this.$bvToast.toast(text, {
                title: `Success!`,
                solid: true,
                variant: 'success',
                autoHideDelay: 1500,
            });
        },
        errorToast(text) {
            if (typeof text == "object") {
                text = text.response.data.msg;
            }

            this.$bvToast.toast(text, {
                title: `Error!`,
                solid: true,
                variant: 'danger',
                autoHideDelay: 4000,
            });
        }
    }
})

/* eslint-disable no-new */
const app = new Vue({
    el: '#app',
    router: router,
    store,
    //mode: "history",
    template: '<App/>',
    sockets: {
        operation(dataFromServer) {
            this.successToast('New movement available in "My Wallet"!');
        },
        operation_unavailable(destUser) {
            axios
                .post("/api/notify", {
                    mail: destUser,
                })
                .then(response => {
                })
                .catch(error => {
                    this.errorToast(error);
                });
        },
    },
    components: {
        App
    },
    
    created() {
        this.$store.commit("loadSession");
    }
})
