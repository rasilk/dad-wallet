import DashboardLayout from "../layout/dashboard/DashboardLayout.vue";
// GeneralViews
import NotFound from "../pages/NotFoundPage.vue";

// Admin pages


import Login from "../pages/Login.vue";
import Income from "../pages/Income.vue";
import Expense from "../pages/Expense.vue";
import CreateManager from "../pages/CreateManager.vue";
import Welcome from "../pages/Welcome.vue";
import EditMovement from "../pages/EditMovement.vue";
import Register from "../pages/Register.vue";
import Recover from "../pages/Recover.vue";
import Statistics from "../pages/Stats.vue";
import Dashboard from "../pages/Dashboard.vue";
import UserProfile from "../pages/UserProfile.vue";
import Wallet from "../pages/Wallet.vue";
import Users from "../pages/Users.vue";


const routes = [
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/register",
    name: "register",
    component: Register
  },
  {
    path: "/recover",
    name: "recover",
    component: Recover
  },
  {
    path: "/welcome",
    name: "welcome",
    component: Welcome
  },
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/dashboard",
    children: [
      {
        path: "stats",
        name: "Statistics",
        component: Statistics
      },
      {
        path: "dashboard",
        name: "dashboard",
        component: Dashboard
      },
      {
        path: "profile",
        name: "profile",
        component: UserProfile
      },
      {
        path: "wallet",
        name: "Wallet",
        component: Wallet
      },
      {
        path: "movement",
        name: "Edit Movement",
        props: true,
        component: EditMovement
      },
      {
        path: "expense",
        name: "expense",
        component: Expense
      },
      {
        path: "create",
        name: "create",
        component: CreateManager
      },
      {
        path: "users",
        name: "users",
        component: Users
      },
      {
        path: "income",
        name: "Income",
        component: Income
      },
    ]
  },
  
  

];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
