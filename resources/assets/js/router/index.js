import Vue from "vue";
import VueRouter from "vue-router";
import routes from "./routes";

import store from "../stores/global-store";

Vue.use(VueRouter);

// configure router
const router = new VueRouter({
    mode: 'history',
    routes: routes, // short for routes: routes
    linkActiveClass: "active"
});

router.beforeEach((to, from, next) => {
    store.commit("loadSession");

    //caso tente sair limpa o user e redirecciona para login
    if (to.path == "/logout") {
      //this.$socket.emit("user_exit", this.$store.state.id);
      store.commit("clearSession");
      next("/");
      return;
    }

    //caso tente aceder a alguma pagina sem ser estas, volta para login
    if (!store.state.user && (to.path != "/welcome" && to.path != "/login" && to.path != "/logout" && to.path != "/register" && to.path != "/recover")) {
        next("/welcome");
        return;
    }
    
    //caso tente aceder a uma destas pagina sem login, vai para as paginas
    if (!store.state.user && (to.path == "/welcome" || to.path == "/login" || to.path == "/logout" || to.path != "/register" || to.path == "/recover")) {
        next();
        return;
    }

    //caso ja tenha o login feito e tente aceder a uma destas, volta para a home
    if (store.state.user && (to.path == "/login" || to.path == "/register" || to.path == "/recover" || to.path == "/welcome")) {
        next("/");
        return;
    }

    next();
});

export default router;
