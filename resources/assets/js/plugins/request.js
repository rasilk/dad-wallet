import axios from 'axios'

const Request = axios.create({
  baseURL: process.env.BASE_URL,
  timeout: 100000,
  transformResponse: [
    function (data) {
      return data
    }
  ]
})

// standard.interceptors.response.use(..., ...)

export default Request