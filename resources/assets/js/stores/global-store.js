/*jshint esversion: 6 */

import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        token: "",
        user: "",
        id: ""
    },
    mutations: {
        clearSession: state => {
            state.user = "";
            state.token = "";
            state.id = "";
            localStorage.removeItem("user");
            localStorage.removeItem("token");
            localStorage.removeItem("id");
            axios.defaults.headers.common.Authorization = undefined;
        },
        setUser: (state, user) => {
            state.user = user;
            localStorage.setItem("user", user);
        },
        setToken: (state, token) => {
            state.token = token;
            localStorage.setItem("token", token);
            axios.defaults.headers.common.Authorization = "Bearer " + token;
        },
        setId: (state, id) => {
            state.id = id;
            localStorage.setItem("id", id);
        },
        loadSession: state => {
            state.token = "";
            state.user = "";
            state.id = "";
            let token = localStorage.getItem("token");
            let user = localStorage.getItem("user");
            let id = localStorage.getItem("id");
            if (token) {
                state.token = token;
                axios.defaults.headers.common.Authorization = "Bearer " + token;
            } else {
                axios.defaults.headers.common.Authorization = undefined;
            }
            if (user) {
                state.user = user;
            }

            if (id) {
                state.id = id;
            }
        },
    }
});
