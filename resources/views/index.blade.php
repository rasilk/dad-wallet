<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!--<link rel="icon" type="image/png" sizes="96x96" href="<%= webpackConfig.output.publicPath %>favicon.png">-->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <title>Virtual Wallet</title>
  <!-- Bootstrap core CSS     -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">

  <!--  Fonts and icons     -->
  <link type="text/css" href="https://fonts.googleapis.com/css?family=Muli:400,300" rel="stylesheet">
  <link type="text/css" href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>

<div class="wrapper" id="app">

</div>
<script src="js/main.js"></script>

</body>

</html>


