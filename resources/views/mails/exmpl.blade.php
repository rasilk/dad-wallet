@component('mail::message')
Hello **{{$name}}**,  {{-- use double space for line break --}}
New activity in your account:
{{$event}} just happen!
{{--Click below to start working right now
@component('mail::button', ['url' => $link])
Go to your inbox
@endcomponent --}}
Sincerely,  
DAD team.
@endcomponent