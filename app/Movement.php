<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movement extends Model
{
    public $timestamps = false;

    protected $fillable = [
        "email", "balance", "created_at",  "id"
    ];


    public function trasnfer_wallet_id()
    {
        return $this->belongsTo('App\Wallet');
    }

    public function trasnfer_movement_id()
    {
        return $this->belongsTo('App\Movement');
    }

    public function category_id()
    {
        return $this->belongsTo('App\Category');
    }

}
