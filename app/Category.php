<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function moviment()
    {
        return $this->hasMany('App\Movement');
    }
}