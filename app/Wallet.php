<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $fillable = [
        "email", "balance", "created_at", "updated_at", "id"
    ];

    public function email()
    {
        return $this->belongsTo('App\User');
    }
    public function id()
    {
        return $this->belongsTo('App\User');
    }
    public function moviment()
    {
        return $this->hasMany('App\Movement');
    }

}
