<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Mailtrap extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name , $event)
    {
        $this->name= $name;
        $this->event= $event;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('mail@example.com', 'Mailtrap')
            ->subject('New DAD notification')
            ->markdown('mails.exmpl')
            ->with([
                'name' => $this->name,
                'link' => 'https://mailtrap.io/inboxes',
                'event' => $this->event
            ]);
    }
}
