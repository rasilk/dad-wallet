<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class MailControllerAPI extends Controller
{
    public function notify(Request $request){
        
        $validator = Validator::make($request->all(), [
            'mail' => 'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => true, 'msg' => $validator->errors()->first()], 400);
        }

        Mail::raw('New movement available in "My Wallet".', function ($message) use ($request) {
            $message->from(config("mail.from.address"), config("mail.from.name"));
            $message->subject("New movement :: Virtual Wallet");
            $message->to($request->get("mail"));
        });
        
        return response()->json(['error' => false, 'msg' => "Mail sent"], 200);
    }
}
