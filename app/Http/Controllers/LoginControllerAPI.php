<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Auth;

use App\User;

class LoginControllerAPI extends Controller
{
    public function login(Request $request)
    {

        $http = new Client;

       //$user = User::where('id',61)->firstOrFail();
       // $user->password = Hash::make("secret");
       // $user->save();
     //   dd( $user);
       // dd(User::where('id', 1)->firstOrFail());
    //  dd(Hash::make("secret"));
        $response = $http->post(config("server.server_url") . '/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => config("server.client_id"),
                'client_secret' => config("server.client_secret"),
                'username' => $request->email,
                'password' => $request->password,
                'scope' => ''
            ],
            'exceptions' => false,
        ]);
        $errorCode = $response->getStatusCode();
        if ($errorCode != 200) {
            return response()->json(['msg' => 'User credentials are invalid'],$errorCode);
        }

        $data = json_decode((string) $response->getBody(), true);

        $user = User::where("email", $request->email);

        $id = null;

        if ($user->exists()){
            $id = $user->first("id")->id;
        }

        return response()->json(['error' => false, 'msg' => 'Logged with success', 'id' => $id, 'access_token' => $data["access_token"]], 200);
    }

    public function logout()
    {
        Auth::guard('api')->user()->token()->revoke();
        Auth::guard('api')->user()->token()->delete();
        return response()->json(['msg' => 'Token revoked'], 200);
    }

    public function invalid(){
        if (Auth::guard('api')->user()){
            return redirect()->route('profile');
        }

        return response()->json(['error' => true, 'msg' => 'Not logged in'], 200);
    }
}
