<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Wallet;
use GuzzleHttp\Client;
use File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
class RegisterControllerAPI extends Controller
{
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'name' => 'required|regex:/^[\p{Latin} ]+$/u',
            'nif' => 'required|numeric|digits:9|unique:users,nif',
            'password' => 'required|string|min:3',
            'repeat_new_password' => 'required|string|min:3',
            'photo' => 'nullable|mimes:jpeg,jpg'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => true, 'msg' => $validator->errors()->first()], 400);
        }
        if( $request["password"] != $request["repeat_new_password"]   ){
            return response()->json(['error' => true, 'msg' => 'New Password are different from new password repeated'], 400);
        }

        $user = new User();
        $user->name = $request["name"];
        $user->email = $request["email"];
        $user->nif = $request["nif"];
        $user->type = "u";
        $user->password = Hash::make($request["password"]);
        $user->save(); //gerar primary key

        if($request["photo"] != null){
            File::delete(storage_path('app/public/fotos/') . $user->photo);
            $filename = $user->id . '_' . uniqid() . '.' . $request->file("photo")->getClientOriginalExtension();
            $request->file("photo")->move(storage_path('app/public/fotos'), $filename);
            $user->photo = $filename;
        }
        $user->save();

        $wallet = new Wallet();
        $wallet->id = $user->id;
        $wallet->email = $user->email;
        $wallet->balance = 0;
        $wallet->save();

        $http = new Client;
        $response = $http->post(config("server.server_url") . '/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => config("server.client_id"),
                'client_secret' => config("server.client_secret"),
                'username' => $request->email,
                'password' => $request->password,
                'scope' => ''
            ],
            'exceptions' => false,
        ]);
        $errorCode = $response->getStatusCode();
        $token = null;
        if ($errorCode == 200) {
            $token = json_decode((string) $response->getBody(), true)["access_token"];
        }

        return response()->json(['error' => false, 'msg' => 'User Created with Success', 'access_token' => $token], 200);
    }


    public function createByAdmin(Request $request)
    {

      
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'name' => 'required|string|max:50',
            'password' => 'required|string|min:3',
            'type' => 'required|in:o,a',
            'repeat_new_password' => 'required|string|min:3',
            'photo' => 'nullable|mimes:jpeg,jpg'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => true, 'msg' => $validator->errors()->first()], 400);
        }
        if( $request["password"] != $request["repeat_new_password"]   ){
            return response()->json(['error' => true, 'msg' => 'New Password are different from new password repeated'], 400);
        }

        $user = new User();
        //$user->save(); //gerar primary key
    


        $user->name = $request["name"];
        $user->email = $request["email"];
        $user->type =  $request["type"];
        $user->password = Hash::make($request["password"]);

      

        $user->save();
    //    $user->save();

        
        if($request["photo"] != null){
        File::delete(storage_path('app/public/fotos/') . $user->photo);
        $filename = $user->id . '_' . uniqid() . '.' . $request->file("photo")->getClientOriginalExtension();
        $request->file("photo")->move(storage_path('app/public/fotos'), $filename);
        $user->photo = $filename;
    }
    $user->save();
        return response()->json(['error' => false, 'msg' => 'User Created with Success'], 200);
    }


}

/*US 2. As an anonymous user I want to create an account with an associated virtual wallet by
registering myself on the platform. Registration data should include 
user’s name (only spaces and letters)
e-mail (must be unique)
password (3 or more characters)
NIF (fiscal identification) a
optional photo (by uploading an image)
When the user's account is created it will be automatically associated with a virtual wallet with the balance value of zero./*

