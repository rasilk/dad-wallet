<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Http\Resources\ManagerUsersResource;
use App\Http\Resources\WalletResource;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Auth;
use App\Wallet;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use File;

class CategoryControllerAPI extends Controller
{
    public function index(Request $request){
        return response()->json(['error' => false, 'msg' => "All categories", 'data' => Category::get(["type", "name"])], 200);
    }
}
