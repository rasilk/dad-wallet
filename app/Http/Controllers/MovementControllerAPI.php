<?php

namespace App\Http\Controllers;

use App\Mail\Mailtrap;
use Illuminate\Support\Facades\Mail;
use App\Movement;
use App\Wallet;
use App\User;
use App\Category;
use App\Http\Resources\ManagerUsersResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class MovementControllerAPI extends Controller
{
    public function store(Request $request)
    {

        //dd($request["category"] );
        if ($request["iban"] != null) {
            $request["iban"] = str_replace(' ', '', $request["iban"]); //tirar espaços iban
        }
        if ($request["category"] != null) {
            $cat =   Category::where('name', $request['category'])->firstOrFail();
            // $request["category_id"] =    $cat->id;
            //fazer get id da DB
        }
        // dd($cat->id);
        $date = Carbon::now();
        $save_date = $date->format("Y-n-j H:m:s");

        //     dd($date->format("Y-n-j H:m:s"));

        $validator = Validator::make($request->all(), [
            'type' => 'required|in:e,i',
            'category' => 'nullable|required_if:type,==,e|between:1,29',
            'description' => 'nullable|required_if:type,==,e',
            'email' => 'required_if:type,==,i|exists:wallets,email',
            'type_payment' => 'nullable|required_if:transfer,==,0|in:c,bt,mb',
            'transfer' => 'required_if:type,==,e|in:0,1',
            //            'date' => 'required|date|before:tomorrow',
            'value' => 'required|numeric|min:0.01|max:5000',
            'transfer_wallet_id' => 'nullable|required_if:transfer,==,1|exists:wallets,email',
            'source_description' => 'nullable|required_if:transfer,==,1',
            'iban' => 'nullable|required_if:type_payment,==,bt|regex:/^[A-Z]{2}[1-9]{23}$/',
            'mb_entity_code' => 'nullable|required_if:type_payment,==,mb|digits:5', //e se for multibanco e nao for "e"
            'mb_payment_reference' => 'nullable|required_if:type_payment,==,mb|digits:9', //e se for multibanco e nao for "e"


        ]);


        if ($validator->fails()) {
            return response()->json(['error' => true, 'msg' => $validator->errors()->first()], 400);
            //return response()->json(['error' => true, 'msg' => 'You should specify the type of movement'], 400);
        }

        /*
        if ($request["transfer"] == "1"){//SE FOR TRANSFERENCIA
                if ($request["transfer_wallet_id"] != null ||  $request["type_payment"] != null ) {
                    return response()->json(['error' => true, 'msg' => 'Some input field are incorrect, this is a transfer.'], 400);
                    //return response()->json(['error' => true, 'msg' => 'You should specify the type of movement'], 400);
                }  
        }*/



        /*if($request["type_payment"] == "bt"){{
            if ($request["source_description"] != null){}
                return response()->json(['error' => true, 'msg' => 'Source description is only accepted in Incomes!'], 400);
            }
            
        }*/
        if (Auth::guard('api')->user()->type == "u") {
            //     if ($request["type"] == "e"){
            /*  if($request["email"] != Auth::guard('api')->user()->email ){
                        return response()->json(['error' => true, 'msg' => "The source wallet doesent bellong to the current user"], 400);
                    }*/
            $wallet_current = Wallet::where('email',  Auth::guard('api')->user()->email)->firstOrFail();


            $movement = new movement();
            $movement->type = "e";
            // $movement->type_payment = $request["type_payment"];
            $movement->description = $request["description"];
            $movement->category_id =  $cat->id;
            $movement->wallet_id = $wallet_current->id;

            $movement->start_balance =  $wallet_current->balance;
            $movement->value = $request["value"];
            $wallet_current->balance = $wallet_current->balance - $request["value"];
            $movement->end_balance =  $wallet_current->balance;
            $movement->date  = $save_date;


            if (Auth::guard('api')->user()->type == "o") {  //FAZ OS só type = "i"
                return response()->json(['error' => true, 'msg' => 'Operator only make income movements'], 400);
            }

            if ($request["transfer"] == 0) { //payment to external entity
                $movement->transfer = 0;
                $movement->type_payment = $request["type_payment"];
                if ($request["type_payment"] == "mb") {
                    $movement->mb_entity_code = $request["mb_entity_code"];
                    $movement->mb_payment_reference = $request["mb_payment_reference"];
                } else {
                    $movement->iban = $request["iban"];
                }
                $wallet_current->save();

                $movement->save();
            } else {
                //é transferencia!!!!
                $wallet_destinantion = Wallet::where('email', $request["transfer_wallet_id"])->firstOrFail();

                if ($request["email"] == $wallet_destinantion->email) {
                    return response()->json(['error' => true, 'msg' => "Cant transfer money to their own wallet"], 400);
                }

                if ($request["transfer_wallet_id"] == Auth::guard('api')->user()->email) {
                    return response()->json(['error' => true, 'msg' => "Cant transfer money to their own wallet"], 400);
                }

                $movement->transfer = 1;
                //    $movement->source_description = $request["source_description"];
                $movement->transfer_wallet_id = $wallet_destinantion->id;

                $wallet_current->save();

                $movement->save();

                //criar o segundo movimento par
                $movement_dest = new movement();


                $movement_dest->type = "i";
                $movement_dest->transfer = 1;

                $movement_dest->source_description = $request["source_description"];
                $movement_dest->wallet_id = $wallet_destinantion->id;

                $movement_dest->start_balance =  $wallet_destinantion->balance;

                $movement_dest->value = $request["value"];
                $wallet_destinantion->balance = $wallet_destinantion->balance + $request["value"];
                $movement_dest->end_balance =  $wallet_destinantion->balance;
                $movement_dest->date  = $save_date;
                //   dd($movement->id);
                $movement_dest->category_id =  $cat->id;
                // dd("chegou");
                $wallet_destinantion->save();
                $movement_dest->save();

                /// dd($movement->id);
                //$movmiment_first = Moviment::where('email', $request["transfer_wallet_id"])->firstOrFail();
                //$movmiment_second = Wallet::where('email', $request["transfer_wallet_id"])->firstOrFail();
                $movement->transfer_movement_id = $movement_dest->id;
                $movement->transfer_wallet_id = $wallet_destinantion->id;

                $movement_dest->transfer_movement_id = $movement->id;
                $movement_dest->transfer_wallet_id = $wallet_current->id;

                $movement_dest->save();
                $movement->save();
                // dd( $movement->id );
                //  $movement->save();

                //  $movement->transfer_movement_id = ($movement->id)+1;
                //temos de criar uma nova para mandar dinheiro para os outros, esta fica com transfer_movement_id = id do movimento +1 , e a outra com movimento -1;
                //  return response()->json(['error' => false, 'msg' =>  Movement::where('type' , 'e')->orderBy('transfer_movement_id', 'desc')->first()], 200);
            }



            return response()->json(['error' => false, 'msg' => 'Moviment Created with Success'], 200);



            //   return response()->json(['error' => true, 'msg' => 'Operator only make income movements'], 400);
        }


        // }
        //dd("asd");
        else if (Auth::guard('api')->user()->type == "o") { //OPERATOR MOVEMENT
            //AeronavePiloto::where('matricula', $aeronave)->where('piloto_id', $request->piloto)->get()
            // dd("chefou aaqui");

            $wallet_current = Wallet::where('email', $request["email"])->firstOrFail();
            // dd($wallet_current );

            // dd( $cat->id);
            $movement = new movement();
            $movement->type = "i";
            $movement->transfer = 0;
            $movement->date  = $save_date;
            //category id ????
            $movement->wallet_id = $wallet_current->id;
            $movement->start_balance =  $wallet_current->balance;
            $movement->value = $request["value"];
            $wallet_current->balance = $wallet_current->balance + $request["value"];

            $movement->end_balance =  $wallet_current->balance;
            $movement->type_payment = $request["type_payment"];
            if ($request["category"] != null) {
                $movement->category_id =  $cat->id;
            }
            if ($request["type_payment"] == "bt") {

                $movement->iban = $request["iban"];
            }
            $movement->save();
            $wallet_current->save();

            return response()->json(['error' => false, 'msg' => 'Moviment Created with Success'], 200);
        }

        return response()->json(['error' => true, 'msg' => 'Something went wrong, movement not created'], 400);
        // return response()->json(['error' => false, 'msg' => 'Moviment Created with Success'], 200);

    }





    public function showMoviment($moviment)
    {
        // dd($moviment);

        $moviment_show = Movement::where("id", $moviment)->firstOrFail();

        if ($moviment_show->wallet_id != Auth::guard('api')->user()->id) {
            return response()->json(['error' => true, 'msg' => "The source movement doesent bellong to the current user"], 400);
        }
        if ($moviment_show->category_id != null) {
            $category = Category::where("id", $moviment_show->category_id)->firstOrFail();
            $moviment_show->category_id =  $category->name;
        }
        //VER ESTA PARTE!!!!
        // return response()->json(['error' => false, 'data' => ManagerUsersResource::collection(User::paginate(15))], 200);
        //   dd(ManagerUsersResource::collection(User::paginate(5)));
        //make(User::paginate(15)));
        //    Mail::to("fernando@mailtrap.pt")->send(new Mailtrap("joel" ,"recebeu dinheiro")); 

        return response()->json(['error' => false, 'data' => $moviment_show], 200);
    }





    public function editMoviment(Request $request, $moviment)
    {
        $moviment_show = Movement::where("id", $moviment)->firstOrFail();
        if ($moviment_show->wallet_id != Auth::guard('api')->user()->id) {
            return response()->json(['error' => true, 'msg' => "The source movement doesent bellong to the current user"], 400);
        }


        if ($moviment_show->type == "e") {
            $validator = Validator::make($request->all(), [

                'category' => 'required|string',
                'description' => 'required',
            ]);
        } else {
            $validator = Validator::make($request->all(), [

                'category' => 'nullable|string',
                'description' => 'required',
            ]);
        }
        if ($validator->fails()) {
            return response()->json(['error' => true, 'msg' => $validator->errors()->first()], 400);
            //return response()->json(['error' => true, 'msg' => 'You should specify the type of movement'], 400);
        }

        $moviment_show->description = $request["description"];

        if ($request["category"] != null) {
            // $moviment_show->category_id =  $cat->id;

            $category = Category::where("name", $request["category"])->firstOrFail();

            $moviment_show->category_id =  $category->id;
        }
        $moviment_show->save();

        return response()->json(['error' => false, 'msg' => 'Movement edited with sucess'], 200);
    }


    public function sendEmail($user, $event)
    {

        $user_current =  User::where("email", $user)->firstOrFail();
        Mail::to($user_current->email)->send(new Mailtrap($user_current->name, $event));
    }






    public function show(Request $request)
    {
        $wallet = Wallet::where("email", Auth::guard('api')->user()->email)->first("id");

        $movements = Movement::where("wallet_id", $wallet->id)->orderBy('date', 'DESC')->get();

        return response()->json(['error' => false, 'data' => $movements], 200);
    }



    public function statistics(Request $request)
    {

        if (Auth::guard('api')->user()->type == "u") {


            $movemets =  Movement::where("wallet_id", Auth::guard('api')->user()->id);

            /*  $expanse = $movemets->where("type", "e")->count();
        $income =  $movemets->where("type", "i")->count();
        $bt = $movemets->where("type_payment", "bt")->count();
        $c = $movemets->where("type_payment", "c")->count();
        $mb =$movemets->where("type_payment", "mb")->count();

        */
            $expanse = Movement::where("wallet_id", Auth::guard('api')->user()->id)->where("type", "e")->count();
            $income = Movement::where("wallet_id", Auth::guard('api')->user()->id)->where("type", "i")->count();
            $bt = Movement::where("wallet_id", Auth::guard('api')->user()->id)->where("type_payment", "bt")->count();
            $c = Movement::where("wallet_id", Auth::guard('api')->user()->id)->where("type_payment", "c")->count();
            $mb = Movement::where("wallet_id", Auth::guard('api')->user()->id)->where("type_payment", "mb")->count();

            $cats =  Category::All();
            $cat_stats = [];

            $item = 0;
            foreach ($cats as $cat) {
                $cat_stats[] = Movement::where("category_id", $cat->id)->where("wallet_id", Auth::guard('api')->user()->id)->count();
            }

            $stats = ["income" => $income, "expanse" => $expanse, "bt" => $bt, "c" => $c,  "mb" => $mb, "category" => $cat_stats];
            return response()->json(['error' => false, 'data' =>  $stats], 200);
        } else if (Auth::guard('api')->user()->type == "a") {
            $expanse = Movement::where("type", "e")->count();
            $income = Movement::where("type", "i")->count();
            $bt = Movement::where("type_payment", "bt")->count();
            $c = Movement::where("type_payment", "c")->count();
            $mb = Movement::where("type_payment", "mb")->count();

            $cats =  Category::All();
            $cat_stats = [];

            $item = 0;
            foreach ($cats as $cat) {
                $cat_stats[] = Movement::where("category_id", $cat->id)->count();
            }



            $stats = ["income" => $income, "expanse" => $expanse, "bt" => $bt, "c" => $c,  "mb" => $mb, "category" => $cat_stats];
            return response()->json(['error' => false, 'data' =>  $stats], 200);
        }
    }
}
