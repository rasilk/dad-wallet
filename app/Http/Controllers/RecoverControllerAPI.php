<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Wallet;
use GuzzleHttp\Client;
use File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class RecoverControllerAPI extends Controller
{
    public function recover(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => true, 'msg' => $validator->errors()->first()], 400);
        }

        $user = User::where('email', request()->input('email'))->first();
        /*
        $token = Password::getRepository()->create($user);
        $user->sendRes($token);
        $user->getEmail*/
        $user->sendResetLink($request->only('email'));
        //$user->getEmailForPasswordReset();

        return response()->json(['error' => false, 'msg' => 'Email sent. Please check your inbox.'], 200);
    }
}