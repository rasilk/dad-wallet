<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Http\Resources\ManagerUsersResource;
use App\Http\Resources\WalletResource;
use App\Http\Resources\MovementResource;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Auth;
use App\Wallet;
use App\Movement;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use File;

class UserControllerAPI extends Controller
{

    public function show(Request $request)
    {
        // $user = User::where('email', $request["email"])->firstOrFail();
        //if($user->id != Auth::guard('api')->user()->id){
        //    return response()->json(['error' => true, 'msg' => "That user arent currentry connected"], 400);
        //}   
        return response()->json(['error' => false, 'data' => UserResource::make(Auth::guard('api')->user())], 200);
    }

    public function balance(Request $request)
    {
        // $user = User::where('email', $request["email"])->firstOrFail();
        //if($user->id != Auth::guard('api')->user()->id){
        //    return response()->json(['error' => true, 'msg' => "That user arent currentry connected"], 400);
        //}   
       // $queries["wallet_id"] = Auth::guard('api')->user()->id;
        return response()->json(['error' => false, 'data' => WalletResource::make(Wallet::where( 'id' , Auth::guard('api')->user()->id )->firstOrFail())], 200);
    }

    public function showUsersByAdmin(Request $request)
    {


        $filters = ['type', 'name', 'email', 'active'];
        $queries = array();

        foreach ($filters as $filter) {
            if ($request[$filter] != null) {
                    $queries[$filter] = $request[$filter];
  
            }
        }


        

  //      return response()->json(['error' => false, 'data' => ManagerUsersResource::collection(User::where($queries)->paginate(15),   'meta')], 200);


   return ManagerUsersResource::collection(User::where($queries)->paginate(15));
    //    return ManagerUsersResource::collection(user::paginate(15));
        //  return ManagerUsersResource::collection(User::all());

    }

    public function showWallet(Request $request)
    {
      //  $filters = ['id', 'type', 'category_id', 'type_payment', 'trasnfer_wallet_id'];
 
        $validator = Validator::make($request->all(), [
            'type' => 'nullable|in:e,i',
            'id' => 'nullable|numeric',
            'category_id' => 'nullable',
            'type_payment' => 'nullable|in:c,bt,mb',
            'dataInit' => 'nullable|date',
            'dataEnd' => 'nullable|date',
            'transfer_wallet_id' => 'nullable|exists:wallets,email',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => true, 'msg' => $validator->errors()->first()], 400);
        }


        if($request["category_id"] != null){
            $category = Category::where("name", $request["category_id"])->firstOrFail();
            $request["category_id"] =$category->id; 
        }

        $filters = ['id', 'type', 'category_id', 'type_payment', 'trasnfer_wallet_id'];
        $queries = array();

        foreach ($filters as $filter) {
            if ($request[$filter] != null) {
                    $queries[$filter] = $request[$filter];
  
            }
        }
        
        $queries["wallet_id"] = Auth::guard('api')->user()->id;

if($request['dataInit'] != null &&  $request['dataEnd']  != null){
   //dd($request['dataEnd'] );
   $dataInit = new DateTime();
   $dataEnd = new DateTime();

   $dataInit = $request['dataInit'];
   $dataEnd = $request['dataEnd'];
  // dd($request['dataInit']  . " e a final " .  $request['dataEnd'] );
  $movements =  MovementResource::collection( Movement::where( $queries)->WhereBetween('date', [ $dataInit , $dataEnd ])->orderBy('date', 'desc')->paginate(15));
  //$movements = (new MovementResource(Movement::where( $queries)->WhereBetween('date', [ $dataInit , $dataEnd ])->orderBy('date', 'desc')))->paginate(15);
  //  $movements =  Movement::where( $queries)->WhereBetween('date', [ $dataInit , $dataEnd ])->orderBy('date', 'desc')->paginate(15);
}else{

    
   //return ManagerUsersResource::collection(User::where($queries)->paginate(15));
   $movements = MovementResource::collection( Movement::where( $queries)->orderBy('date', 'desc')->paginate(15));
 //  $movements = (new MovementResource(Movement::where( $queries)->orderBy('date', 'desc')))->paginate(15);
}


        $queries["wallet_id"] = Auth::guard('api')->user()->id;
       

       return  MovementResource::collection( Movement::where( $queries)->orderBy('date', 'desc')->paginate(15));
    
      //  $users = User::where($queries)->sortable()->paginate(25);

        //$user =  User::where('id', $request['user'])->firstOrFail();
      // return MovementResource::collection(Movement::paginate(5));
     // return response()->json(['error' => false,  'meta' => MovementResource::collection(Movement::paginate(5))->get('links'),  'data' => MovementResource::collection(Movement::paginate(5))], 200);
      //'balance' => $this->balance,
      //'email' => Auth::guard('api')->user()->email,
      
    //  return response()->json(['error' => false, 'data' => WalletResource::make(Wallet::where( $queries)->firstOrFail())], 200);
    }


    
    public function deleteByAdmin(Request $request)
    {
        $userDelete =  User::where('id', $request['user'])->firstOrFail();
        if ($userDelete->type == "a" || $userDelete->type == "o") {
            if (($userDelete->email) == (Auth::guard('api')->user()->email)) {

                return response()->json(['error' => true, 'msg' => 'Cannot remove their own user'], 400);
            } else {

                $userDelete->delete();
                return response()->json(['error' => false, 'msg' => 'Deleted with Sucess'], 200);
            }
        }
        //************************DISABLE USERS ****************************//
        if ($userDelete->active == 1) {


            $wallet = Wallet::where("email", $userDelete->email)->first();

            if (!is_null($wallet)) {


                if ($wallet->balance == 0) { //can disable!
                    $userDelete->active = 0;
                    $userDelete->save();
                    return response()->json(['error' => false, 'msg' => 'User Disable with Sucess'], 200);
                } else {
                    return response()->json(['error' => true, 'msg' => 'The ballance of the user is not 0'], 400);
                }
            }
            $userDelete->active = 0;
            $userDelete->save();
            return response()->json(['error' => false, 'msg' => 'User Disable with Sucess'], 200);
        } else {
            $userDelete->active = 1;
            $userDelete->save();
            return response()->json(['error' => false, 'msg' => 'User Enable with Sucess'], 200);
        }
    }

    //    dd("invalidates");
    //INVALIDATE USERS



    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'nullable|string|min:3',
            'new_password' => 'nullable|string|min:3',
            'repeat_new_password' => 'nullable|string|min:3',
            'nif' => 'nullable|numeric|digits:9|unique:users,nif',
            'name' => 'required|regex:/^[\p{Latin} ]+$/u',
            'photo' => 'nullable'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => true, 'msg' => $validator->errors()->first()], 400);
        }

        $user = Auth::guard('api')->user();

        if ($request["old_password"] != null || $request["new_password"] != null || $request["repeat_new_password"] != null) {
            if (!Hash::check($request["old_password"], Auth::guard('api')->user()->password)) {
                return response()->json(['error' => true, 'msg' => 'Invalid old password'], 400);
            }

            $validator = Validator::make($request->all(), [
                'new_password' => 'nullable|string|min:3',
                'repeat_new_password' => 'nullable|string|min:3'

            ]);

            if ($validator->fails()) {
                return response()->json(['error' => true, 'msg' => $validator->errors()->first()], 400);
            }


            if ($request["new_password"] != $request["repeat_new_password"]) {
                return response()->json(['error' => true, 'msg' => 'New Password are different from new password repeated'], 400);
            }

            $user->password = Hash::make($request["repeat_new_password"]);
        }



        //$user->save(); //gerar primary key
        if ($user->type == "u") {
            $validator = Validator::make($request->all(), [
                'nif' => 'required|numeric|digits:9',
            ]);
            if ($validator->fails()) {
                return response()->json(['error' => true, 'msg' => $validator->errors()->first()], 400);
            }
            $user->nif = $request["nif"];
        }
        $user->name = $request["name"];

        $user->save();

        return response()->json(['error' => false, 'msg' => "Successfully updated"], 200);
    }

    public function photo(Request $request){
        $user = Auth::guard('api')->user();
        $validator = Validator::make($request->all(), [
            'photo' => 'required|mimes:jpeg,jpg'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => true, 'msg' => $validator->errors()->first()], 400);
        }

        File::delete(storage_path('app/public/fotos/') . $user->photo);
        $filename = $user->id . '_' . uniqid() . '.' . $request->file("photo")->getClientOriginalExtension();
        $request->file("photo")->move(storage_path('app/public/fotos'), $filename);

        $user->photo = $filename;
        $user->save();
        return response()->json(['error' => false, 'msg' => "Photo Successfully Updated", 'filename' => $filename], 200);
    }
}
