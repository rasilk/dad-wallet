<?php

namespace App\Http\Controllers;

use App\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WalletControllerAPI extends Controller
{
    public function balance(Request $request)
    {
        $wallet = Wallet::where("email", Auth::guard('api')->user()->email)->first("balance");
        
        return response()->json(['error' => false, 'balance' => $wallet->balance, 'email' => Auth::guard('api')->user()->email], 200);
    }

    public function walletsCount(){
        return response()->json(['error' => false, 'wallets' => Wallet::all()->count()], 200);
    }

   

}
