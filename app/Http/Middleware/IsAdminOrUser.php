<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class IsAdminOrUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       
        if(Auth::guard('api')->user()->type != "a" && Auth::guard('api')->user()->type != "u"){
            return response()->json(['error' => false, 'msg' => 'The logged User is not an Administrator or User'], 403);
           }    
        
        return $next($request);
    }
}
