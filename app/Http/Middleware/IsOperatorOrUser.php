<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class IsOperatorOrUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       
        if(Auth::guard('api')->user()->type != "o" && Auth::guard('api')->user()->type != "u"){

            return response()->json(['error' => false, 'msg' => 'The logged User is not an Operator or User'], 403);
           }    
        
        return $next($request);
    }
}
