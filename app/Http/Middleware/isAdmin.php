<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       
        if( Auth::guard('api')->user()->type != "a"){
            return response()->json(['error' => false, 'msg' => 'The logged User is not an Administrator'], 403);
           }    
        
        return $next($request);
    }
}
