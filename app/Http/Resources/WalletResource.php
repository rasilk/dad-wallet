<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Movement;
use App\Category;
use Auth;
use DateTime;
use App\Http\Resources\MovementResource;

class WalletResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        if ($request["category_id"] != null) {
            $category = Category::where("name", $request["category_id"])->firstOrFail();
            $request["category_id"] = $category->id;
        }

        $filters = ['id', 'type', 'category_id', 'type_payment', 'trasnfer_wallet_id'];
        $queries = array();

        foreach ($filters as $filter) {
            if ($request[$filter] != null) {
                $queries[$filter] = $request[$filter];
            }
        }

        $queries["wallet_id"] = Auth::guard('api')->user()->id;

        if ($request['dataInit'] != null &&  $request['dataEnd']  != null) {
            //dd($request['dataEnd'] );
            $dataInit = new DateTime();
            $dataEnd = new DateTime();

            $dataInit = $request['dataInit'];
            $dataEnd = $request['dataEnd'];
            // dd($request['dataInit']  . " e a final " .  $request['dataEnd'] );
            $movements = (new MovementResource(Movement::where($queries)->WhereBetween('date', [$dataInit, $dataEnd])->orderBy('date', 'desc')))->paginate(15);
            //  $movements =  Movement::where( $queries)->WhereBetween('date', [ $dataInit , $dataEnd ])->orderBy('date', 'desc')->paginate(15);
        } else {


            //return ManagerUsersResource::collection(User::where($queries)->paginate(15));
            $movements = (new MovementResource(Movement::where($queries)->orderBy('date', 'desc')))->paginate(15);
            //  $movements  =  MovementResource::collection( Movement::where( $queries)->orderBy('date', 'desc')->paginate(15));
            //  $movements =  Movement::where( $queries)->orderBy('date', 'desc')->paginate(15);
        }



        //           $movements= MovementResource::collection(Movement::paginate(5));
        // $movement= Wallet::where("email",$this->email)->first(); 
        return [
            'balance' => $this->balance,
            'email' => Auth::guard('api')->user()->email,
            //  'movements' =>  $movements,



            //   'name' => $this->name,
            //   'email' => $this->email,
            //   'photo' => $this->photo,
            //   'nif' => $this->nif,
        ];

        // return parent::toArray($request->email);
    }
}
