<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use App\User;
use App\Wallet;
use Auth;


class ManagerUsersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

     $wallet=Wallet::where("email",$this->email)->first(); 
     $user = User::where("email",$this->email)->first(); 
     if(!is_null($wallet)){

        if($wallet->balance > 1 ){
            $balance = "Has Money";
        }else{
            $balance = "Empty";
        }
     }else{
        $balance = "No Wallet";
     }

if(!is_null($user)){
    if($user->type == "o"){
        $type = "Operator";
    }elseif ($user->type == "a"){
        $type = "Administrator";
    }else{
        $type = "Wallet User";
    }
}
 if(!is_null($user->photo)){
      $photo_has=  "../storage/fotos/".$user->photo;
  //  $photo_has=  "<img class='avatar border-white' :src=$photo_hase  alt='...'>";
 //$photo_has=  "../storage/fotos/".$user->photo;
 }else{
     $photo_has= null;
 }
        return [
 
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'photo' => $this->photo,    
            'nif' => $this->nif,
            'balance'=> $balance,
            'type'=> $type,
            'active'=> $this->active,
            'photo' => $photo_has
        ];
        //return parent::toArray($request);
    }
}
