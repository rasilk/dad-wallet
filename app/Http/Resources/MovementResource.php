<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Movement;
use App\Wallet;
use App\Category;
use App\User;
use Auth;

class MovementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        //   dd("passou aqui?!");
        $movement = Movement::where("wallet_id", Auth::guard('api')->user()->id)->first();

        if ($this->type == "i") {
            $new_type  = "Income";
        }
        if ($this->type == "e") {
            $new_type   = "Expense";
        }

        if ($this->transfer == 1) {


            $wallet_d = Wallet::Where('id', $this->transfer_wallet_id)->first();
            $user_dest = User::Where('id', $this->transfer_wallet_id)->first();

            if ($user_dest->photo != null) {
                $photo =  "../storage/fotos/" . $user_dest->photo;
            } else {
                $photo = null;
            }

            if ($this->type  == "e") {
                $wallet_dest = "Expanse to wallet: " . $wallet_d->email;
            } else if ($this->type  == "i") {
                $wallet_dest = "Income from wallet: " . $wallet_d->email;
            }
        } else {

            $photo = null;

            $wallet_dest = "Not a Transfer";
        }

        if ($this->category_id != null) {
            $category = Category::where("id", $this->category_id)->firstOrFail();

            $category_name = $category->name;
        } else {
            $category_name = null;
        }


        if ($this->type_payment == "c") {
            $payment_type  = "Cash";
        } else if ($this->type_payment == "bt") {
            $payment_type  = "Bank Transfer";
        } else if ($this->type_payment == "mb") {
            $payment_type  = "MB";
        } else {
            $payment_type  = null;
        }

        return [

            'id' => $this->id,
            'type' => $new_type,
            'type_payment' => $payment_type,
            'wallet_id' => $this->wallet_id,
            'transfer' => $this->transfer,
            'transfer_movement_id' => $this->transfer_movement_id,
            'transfer e-mail' => $wallet_dest,
            'category' => $category_name,
            'iban' => $this->iban,
            'mb_entity_code' => $this->mb_entity_code,
            'mb_payment_reference' => $this->mb_payment_reference,
            'description' => $this->description,
            'source_description' => $this->source_description,
            'photo' => $photo,
            'start_balance' => $this->start_balance,
            'end_balance' => $this->end_balance,
            'value' => $this->value,

            'date' => $this->date,
        ];
        //return parent::toArray($request);
    }
}
