/*jshint esversion: 6 */

//var app = require("http").createServer();

 var app = require('http').createServer(function(req,res){
// Set CORS headers
  res.setHeader('Access-Control-Allow-Origin', '*'); //fazer set por host que utilizemos
  res.setHeader('Access-Control-Request-Method', '*');
  res.setHeader('Access-Control-Allow-Methods', 'UPGRADE, OPTIONS, GET');
  res.setHeader('Access-Control-Allow-Credentials', true);
  //res.setHeader('Access-Control-Allow-Headers', req.header.origin);
  if ( req.method === 'OPTIONS' || req.method === 'UPGRADE' ) {
      res.writeHead(200);
      res.end();
      return;
  }
});

var io = require("socket.io")(app);

var LoggedUsers = require("./loggedusers.js");

app.listen(8080, function() {
  console.log("listening on *:8080");
});

let loggedUsers = new LoggedUsers();

io.on("connection", function(socket) {
  //console.log("client has connected - user ID = " + loggedUsers.userInfoBySocketID(socket.id) + " / socket ID = " + socket.id);
  //console.log("client has connected (socket ID = " + socket.id + ")");

  socket.on('disconnect', function() {
    let user = loggedUsers.userInfoBySocketID(socket.id);
    if (user){
      console.log("Got disconnect! (user ID = " + user + " / socket ID = " + socket.id + ")");
      loggedUsers.removeUserInfoByID(user);
    }
  });

  socket.on("user_enter", function(user) {
    if (user) {
      socket.join(user);
      console.log("socket associated (user ID = " + user + " / socket ID = " + socket.id + ")");
      loggedUsers.addUserInfo(user, socket.id);
    }
  });
  socket.on("user_exit", function(user) {
    if (user) {
      socket.leave(user);
      console.log("socket deassociated (user ID = " + user + " / socket ID = " + socket.id + ")");
      loggedUsers.removeUserInfoByID(user);
    }
  });

  socket.on("operation", function(sourceUser, destUser) {
    let userInfo = loggedUsers.userInfoByID(destUser);
    let socket_id = userInfo !== undefined ? userInfo.socketID : null;
    if (socket_id === null) {
      console.log("unable to send to user ID = " + destUser);
      socket.emit("operation_unavailable", destUser);
    } else {
      console.log("sending to user ID " + destUser + " from " + sourceUser);
      io.to(socket_id).emit("operation", sourceUser);
      //socket.emit("operation_sent", msg, destUser);
    }
  });
});
