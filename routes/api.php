<?php

use Illuminate\Support\Facades\Route;

/* Global */
Route::get('login','LoginControllerAPI@invalid')->name('login');
Route::post('login', 'LoginControllerAPI@login'); //us3
Route::post('register', 'RegisterControllerAPI@create'); //US2
Route::post('recovers', 'RecoverControllerAPI@recover'); //US2
Route::middleware('auth:api')->get('logout','LoginControllerAPI@logout'); //us4

/* Profile */
Route::middleware('auth:api')->get('profile', 'UserControllerAPI@show')->name("profile"); //us5
Route::middleware('auth:api')->patch('profile','UserControllerAPI@update'); //US5
Route::middleware('auth:api')->post('profile/photo', 'UserControllerAPI@photo');

/* Wallet */
Route::middleware('api')->get('wallets', 'WalletControllerAPI@walletsCount'); //usado para homepage
Route::middleware(['auth:api', 'isUser'])->post('wallet', 'UserControllerAPI@showWallet');//us8
Route::middleware(['auth:api', 'isUser'])->get('wallet/balance', 'WalletControllerAPI@balance');//us8 e US 17

/* Statistics */
Route::middleware(['auth:api', 'isAdminOrUser'])->get('statistics', 'MovementControllerAPI@statistics');//us 14

/* Movements */
Route::middleware(['auth:api', 'isUser'])->get('movements', 'MovementControllerAPI@show'); //US7 FAZ ALGUMA COISA?!?!
Route::middleware(['auth:api', 'isOperatorOrUser'])->post('movements', 'MovementControllerAPI@store'); //US6 US9 US10 US13?
Route::middleware(['auth:api', 'isUser'])->get('movements/{moviment}/edit', 'MovementControllerAPI@showMoviment'); //US11
Route::middleware(['auth:api', 'isUser'])->put('movements/{moviment}/edit', 'MovementControllerAPI@editMoviment'); //US11

/* Categories */
Route::middleware('auth:api')->get('categories', 'CategoryControllerAPI@index');
Route::middleware('auth:api')->get('categories/{category}', 'CategoryControllerAPI@filter');

/* Notify User (Socket) */
Route::middleware('auth:api')->post('notify', 'MailControllerAPI@notify');

/* User Management (admin) */
Route::middleware(['auth:api','isAdmin'])->post('users/list', 'UserControllerAPI@showUsersByAdmin');// US16
Route::middleware(['auth:api','isAdmin'])->delete('delete/{user}', 'UserControllerAPI@deleteByAdmin');// US16 FAZ:
Route::middleware(['auth:api','isAdmin'])->post('create', 'RegisterControllerAPI@createByAdmin');  //US15 Delete User se for type = o ou tipe == a, dá disable em user com balance == 0 e dá enable se esse user já tiver disable.


